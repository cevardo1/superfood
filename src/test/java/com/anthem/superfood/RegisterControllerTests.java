package com.anthem.superfood;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import org.junit.Before;
import org.junit.Test;
import org.springframework.test.web.servlet.MockMvc;

import com.anthem.superfood.controllers.RegisterController;

public class RegisterControllerTests {
    private RegisterController rest;
    private MockMvc endpoint;

    @Before
    public void setup() {
        rest = new RegisterController();
        endpoint = standaloneSetup(rest).build();
    }

    @Test
    public void create_ok() throws Exception {
        endpoint
            .perform(post("/registers").contentType(APPLICATION_JSON).content("{\"name\": \"Food Lion\"}"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$").value(hasSize(1)))
            .andExpect(jsonPath("$[0]").value("Food Lion"));

        endpoint
            .perform(post("/registers").contentType(APPLICATION_JSON).content("{\"name\": \"Aldi\"}"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$").value(hasSize(2)))
            .andExpect(jsonPath("$[0]").value("Food Lion"))
            .andExpect(jsonPath("$[1]").value("Aldi"));
    }

    @Test
    public void create_badRequest() throws Exception {
        endpoint
            .perform(post("/registers").contentType(APPLICATION_JSON).content("{\"typoName\": \"Wegmans\"}"))
            .andExpect(status().isBadRequest())
            .andExpect(content().string("'name' is required."));
    }
}