package com.anthem.superfood.controllers;

import static org.springframework.http.ResponseEntity.badRequest;
import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "registers")

public class RegisterController {
    private static final List<String> REGISTERS = new ArrayList<>();

    @RequestMapping(path = "", method = POST)
    public ResponseEntity<?> writeDownRegister(final @RequestBody Map<String, Object> input) {
        if (input.containsKey("name") && input.get("name") != null) {
        	REGISTERS.add(input.get("name").toString());
            return ok(REGISTERS);
        }
        else
            return badRequest().body("'name' is required.");
    }
}
